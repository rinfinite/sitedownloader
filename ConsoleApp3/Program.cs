﻿using CsQuery;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        const string URL = "https://disput.az";
        const string FOLDER = @"C:\Users\username\source\repos\ConsoleApp3\html_parser\";
        public static string data = "";
        public static object lockData = new object();

        static void Main(string[] args)
        {
            while (true)
            {
                deleteFiles();

                List<WaitHandle> threads = new List<WaitHandle>();
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();

                if (httpWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
                    data = streamReader.ReadToEnd();
                    streamReader.Close();

                    CQ cq = CQ.Create(data);

                    foreach (IDomObject element in cq.Find(".news-i"))
                    {
                        string imageUrl = element[0][0][0].GetAttribute("src");
                        string title = element[0][1].Cq().Text();

                        if (imageUrl.Length > 4 & imageUrl.Substring(imageUrl.Length - 4).Equals(".jpg"))
                        {
                            var handle = new EventWaitHandle(false, EventResetMode.ManualReset);
                            Thread thread = new Thread(() =>
                            {
                                string fileName = FOLDER + DateTime.Now.ToString("HHmmssfffffff") + "_" + Thread.CurrentThread.ManagedThreadId + "{0}.jpg";
                                lock (lockData)
                                    data = data.Replace(imageUrl, @"file://" + string.Format(fileName, 0));

                                using (WebClient webClient = new WebClient())
                                {
                                    for (int i = 0; i < 1; i++)
                                    {
                                        webClient.DownloadFile(imageUrl, string.Format(fileName, i));
                                        //Console.WriteLine(string.Format(fileName, i) + " | " + imageUrl);
                                    }
                                }
                                handle.Set();
                            });

                            thread.Start();
                            threads.Add(handle);
                        }
                        else continue;
                    }
                    WaitHandle.WaitAll(threads.ToArray());

                    Console.WriteLine("img before write:" + data.IndexOf("kassa_.jpg"));
                    saveIndexHtml(data);
                    httpWebResponse.Close();
                }
                Console.WriteLine("img:" + data.IndexOf("kassa_.jpg"));
                Console.WriteLine("finished");
                Console.ReadLine();
            }
        }

        static void saveIndexHtml(string data)
        {
            using (FileStream fileStream = new FileStream(FOLDER + "index.html", FileMode.OpenOrCreate))
            {
                byte[] byteArray = Encoding.Default.GetBytes(data);
                fileStream.Write(byteArray, 0, byteArray.Length);

            }
        }
        static void deleteFiles()
        {
            var dir = new DirectoryInfo(FOLDER);
            foreach (FileInfo file in dir.GetFiles())
            {
                file.Delete();
            }
        }
    }
}
